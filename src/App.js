import React from 'react';
import logo from './logo.svg';
import './App.css';
import ReactHLS from 'react-hls';
import VideoPlayer from './VideoPlayer'
class App extends React.Component {
  state = {
    videoURL :'',
    loadVideo: false
  }
  render() {
    return (
      <div className="App">
        <input value={this.state.videoURL} onChange={(e) => this.setState({ videoURL: e.target.value })} />
        <button onClick={() => this.setState({ loadVideo: !this.state.loadVideo })}>{this.state.loadVideo ? 'Stop' : 'Load'}</button>
        {this.state.loadVideo ? 
        <VideoPlayer videoUrl={this.state.videoURL} />
        //<ReactHLS url={this.state.videoURL} width="800" height="1000"/>
        : "Hello"}
      </div>
    );
  }
}

export default App;
