import React from "react";
import videojs from "video.js";
import './App.css'
export default class VideoPlayer extends React.Component {
  player = {};
  constructor(props) {
    super(props);
    this.state = {
      
    };
  }

  playVideo = () => {
    let tempPlayer = videojs("vid");
    tempPlayer.play();
  };

  componentDidMount() {
    var player = videojs("vid", {
      liveui: true,
      preload: "auto",
      controls: true,
      html5: { hls: { overrideNative: true, enableLowInitialPlaylist: true } }
    });
    window.newPlayer = player;
    player.src({
      src: `${this.props.videoUrl}`,
      type: "application/x-mpegURL",
      bandwidth: 102400,
      overrideNative: true,
      enableLowInitialPlaylist: true
    });
    player.play();
  }

  render() {
    return (
      <video-js
        id="vid"
        class="vjs-default-skin vjs-live"
        playsinline
        width="1024px"
        data-setup="{&quot;liveui&quot;: true,controls:false,inline}"
        type='application/x-mpegURL'
      >
      </video-js>
    );
  }
}
